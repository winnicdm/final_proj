import csv

def writeParams(filepath):
    numAgents = [20, 50, 100]
    numCandidates = [2, 3, 10]
    trust = [-0.2, 0, 0.2, 0.4, 0.6, 0.8, 1]
    connectivity = [0.2, 0.4, 0.6, 0.8]
    weightDelta = [0.1, 0.2, 0.3, 0.4]
    MAX_ITERATIONS = 1000

    header = ['agent_count', 'candidate_count', 'system_trust', 'connectivity_percentage', 'weight_change_severity',
              'max_iteration']

    with open(filepath, 'w') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(header)
        for agentCount in numAgents:
            for candidateCount in numCandidates:
                for trustLevel in trust:
                    for connectivityLevel in connectivity:
                        for weightDeltaValue in weightDelta:
                            row = [agentCount, candidateCount, trustLevel, connectivityLevel, weightDeltaValue, MAX_ITERATIONS]
                            writer.writerow(row)
