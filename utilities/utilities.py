# System
import csv
import logging
import numpy as np
from tabulate import tabulate

# Constants
BANNER_WIDTH = 65
SEED = 12345
NUM_TIMES_RUN_PARAMS = 100
LOG_LEVEL = logging.INFO
DEFAULT_FILE_PATH = "./parameters.csv"
DEFAULT_RESULTS_FILE_PATH = "./results.csv"
INPUTS = [
        ["agent_count", "int"],
        ["candidate_count", "int"],
        ["system_trust", "float"],
        ["connectivity_percentage", "float"],
        ["weight_change_severity", "float"],
        ["max_iteration", "int"]
    ]

def generateWeights(systemTrust, numConnections):
    mean = 0.1 * systemTrust/np.sqrt(numConnections)
    sd = 1/np.sqrt(numConnections)
    dist = np.random.normal(mean, sd, numConnections)
    return dist

def print_full_banner_centered(rows):
    print('+' + '-' * BANNER_WIDTH + '+')
    for row in rows:
        print('|' + row.center(BANNER_WIDTH) + '|')
    print('+' + '-' * BANNER_WIDTH + '+')

def print_partial_banner_centered(rows):
    print(('+' + '-' * (BANNER_WIDTH - 20) + '+').center(BANNER_WIDTH))
    for row in rows:
        print(row.center(BANNER_WIDTH))
    print(('+' + '-' * (BANNER_WIDTH - 30) + '+').center(BANNER_WIDTH))

def print_table(rows, head=[]):
    print(tabulate(rows, headers=head))

def print_param_info(paramNum, params):
    rows = []
    for i in range(0, len(params)):
        rows.append([INPUTS[i][0], params[i]])
    print_partial_banner_centered(["Parameter Set " + str(paramNum)])
    print_table(rows)

def print_results(conv_found, cons_found):
    print_table([
        ["Times Convergence Not Found: ", str(NUM_TIMES_RUN_PARAMS-conv_found), str(int(NUM_TIMES_RUN_PARAMS-conv_found/NUM_TIMES_RUN_PARAMS * 100)) + "%"],
        ["Times Convergence Found: ", str(conv_found), str(int(conv_found/NUM_TIMES_RUN_PARAMS * 100)) + "%"],
        ["Times Consensus Found: ", str(cons_found), str(int(cons_found/NUM_TIMES_RUN_PARAMS * 100)) + "%"],
    ])

def print_results_to_file(params, values):
    logging.info('Saving results to the file: ' + DEFAULT_RESULTS_FILE_PATH)
    file = open(DEFAULT_RESULTS_FILE_PATH, "w")
    file.write("agent_count,candidate_count,system_trust,connectivity_percentage,weight_change_severity,max_iteration,total_times_no_convergence,total_times_convergence,total_times_consensus\n")
    for i in range(0, len(params)):
        results_to_write=','.join([str(x) for x in params[i]])
        results_to_write += ','+str(NUM_TIMES_RUN_PARAMS-values[i][0])+','+str(values[i][0])+','+str(values[i][1])+'\n'
        file.write(results_to_write)

    file.close()

def print_usage():
    print_partial_banner_centered([
        "Usage"
    ])
    print("To use this script, simply run it with one of the available optional arguments\n")
    print("python main.py <arg?>\n")
    print("Optional arguments:")
    print_table([
        ["-h", "--help", "Displays this usage message"],
        ["-d", "--default", "Uses all default values for the script and skips setup"],
    ])
    print("\nThe designated file must be a csv file and must contain any number of rows with the following values, deliminated by a comma:")
    print_table(INPUTS)
    print("\nDependencies:")
    print_table([
        ["Python 3.X.X", "The script could work with a lower version, but it was developed using 3"],
        ["tabulate", "Used to generate the dynamic tables seen throughout"],
    ])

def extract_parameters_from_file(path):
    logging.info('Reading parameters from file path: ' + path + '...')
    params = []
    line_count = 0
    with open(path, 'r') as file:
        lines = csv.reader(file)
        headers = next(lines)
        for line in lines:
            if len(line) != len(INPUTS):
                logging.error("Invalid file format, check the csv file for inconsistencies and try again. --help for more information")
                exit()
            params.append(line)
            line_count += 1

    if line_count == 0:
        logging.error("There were no parameters found. Check the file and try again")
        exit()

    set_text = 'set' if line_count == 1 else 'sets' 
    logging.info('DONE! Read ' + str(line_count) + ' ' + set_text + ' of parameters from the file')

    return params

def extract_args(args):
    use_defaults = False
    if len(args) == 2:
        arg = args[1]
        if arg == "-h" or arg == "--help":
            print_usage()
            exit()
        elif arg == "-d" or arg == "--default":
            use_defaults = True
        else:
            logging.error("Invalid arguments provided")
            print_usage()
            exit()
    elif len(args) > 2:
            logging.error("Invalid number of arguments provided")
            print_usage()
            exit()

    return use_defaults