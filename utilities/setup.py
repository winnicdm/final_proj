# Script files
import logging
import utilities.utilities as utils

def run_setup(defaults):
    utils.print_partial_banner_centered([
        "Setup"
    ])
    path = ""

    if not defaults:
        # Grab path
        path = input('Define parameter file path. Default path: \'' + utils.DEFAULT_FILE_PATH + '\'')

    if path == "":
        path = utils.DEFAULT_FILE_PATH
        logging.info('Using default file path: ' + path)

    # read params
    params = utils.extract_parameters_from_file(path)    

    logging.info("Setup complete")

    # return params
    return params