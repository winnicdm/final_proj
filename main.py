# System
import sys
import logging
import traceback
# Script files
import utilities.utilities as utils
import utilities.setup as setup
from models.simulation import Simulation

class Main():
    def run(self):
        param_count = 0
        all_data_results = []
        for param in self.params:
            logging.debug("Executing simulation for parameter set " + str(param_count + 1))
            utils.print_param_info(param_count + 1, param)

            agent_count = int(param[0])
            candidate_count = int(param[1])
            system_trust = float(param[2])
            connectivity_percentage = float(param[3])
            weight_change_severity = float(param[4])
            max_iteration = int(param[5])

            num_times_converge = 0
            num_times_consensus = 0

            logging.info('Simulating Params ' + str(utils.NUM_TIMES_RUN_PARAMS) + ' Times...')
            # Repeat multiple times.
            for i in range(0, utils.NUM_TIMES_RUN_PARAMS):
                sim = Simulation(agent_count, candidate_count, system_trust, weight_change_severity, connectivity_percentage)

                round = 1

                # Simmulate provided parameters
                while(not sim.ConvergenceReached and not sim.ConsensusReached and round <= max_iteration):
                    logging.debug('Simulating round ' + str(round) + '. Repeat #' + str(i) + '...')
                    
                    sim.SimulateRound()

                    round += 1

                num_times_converge += 1 if sim.ConvergenceReached else 0
                num_times_consensus += 1 if sim.ConsensusReached else 0

            logging.info('DONE!')

            # Print results for parameters
            utils.print_results(num_times_converge, num_times_consensus)
            all_data_results.append([num_times_converge, num_times_consensus])

            param_count += 1

        utils.print_results_to_file(self.params, all_data_results)

    def __init__(self):
        utils.print_full_banner_centered([
            "Consensus from Dynamic,",
            "Sparsely Connected,",
            "Distributed Iterative Voting",
            "",
            "Alex Kinnear,",
            "Ben Winslett,",
            "Ian McLachlan"
        ])

        # Get args, if any
        use_defaults = utils.extract_args(sys.argv)

        # Setup
        self.params = setup.run_setup(use_defaults)

if __name__ == '__main__':
    # Setup logging
    logging.basicConfig(
        format='%(asctime)s - %(levelname)s: %(message)s',
        datefmt='%m/%d/%Y %I:%M:%S %p',
        level=utils.LOG_LEVEL)

    try:
        main_program = Main()
        main_program.run()
    except SystemExit:
        logging.info("Terminating...")
    except BaseException as err:
        logging.critical('An unexpected exception occurred: {0}'.format(err))
        logging.debug(traceback.format_exc())