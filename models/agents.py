from random import randint, shuffle

class Agent:
	def __init__(self, id, numCandidates):
		self.id = id
		self.firstVote = randint(0, numCandidates - 1)
		self.currentVote = self.firstVote
		self.connections = {}
		self.numCandidates = numCandidates

	def __repr__(self):
		return f'{self.id}'

	def setConnections(self, numAgents, weights):
		otherAgents = [i for i in range(numAgents)]
		otherAgents.remove(self.id)
		shuffle(otherAgents)
		weights[0] = abs(weights[0])
		s = sum(weights)
		weights = [i/s for i in weights]
		self.connections[self.id] = weights[0]
		weights.pop(0)
		for i in range(len(weights)):
			self.connections[otherAgents[i]] = weights[i]

	def getNextVote(self, agentsDict):
		nextVoteDict = {}
		for i in range(self.numCandidates):
			nextVoteDict[i] = 0
		for ID in self.connections:
			connection = agentsDict[ID]
			nextVoteDict[connection.currentVote] += self.connections[ID]

		nextVote = max(nextVoteDict, key=nextVoteDict.get)

		return nextVote
