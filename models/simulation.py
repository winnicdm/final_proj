import random
import logging
import numpy as np
from models.agents import Agent

class Simulation:
  def __init__(self, numAgents, numCandidates, sysTrust, weightDelta, connPercent):
    self.Agents = {i: Agent(i, numCandidates) for i in range(numAgents)}
    self.Candidates = {i: 0 for i in range(numCandidates)}
    self.WeightDelta = weightDelta
    self.SystemTrust = sysTrust
    self.ConnectionPercent = connPercent
    self.WinningCandidate = None
    self.ConvergenceReached = False
    self.ConsensusReached = False

    numConnections = int((numAgents * connPercent) // 1)
    mean = 0.1 * sysTrust/np.sqrt(numConnections)
    sd = 1/np.sqrt(numConnections)
    dist = np.random.normal(mean, sd, (numAgents, numConnections))

    for agent in self.Agents:
      self.Agents[agent].setConnections(numAgents, dist[agent])
      self.Candidates[self.Agents[agent].firstVote] += 1

  def SimulateRound(self):
    noVotesChanged = True
    newAgentVotes = {}

    # check for each agents new votes
    for agent in self.Agents:
      newAgentVotes[agent] = self.Agents[agent].getNextVote(self.Agents)
      if self.Agents[agent].currentVote != newAgentVotes[agent]:
        self.Candidates[self.Agents[agent].currentVote] -= 1
        self.Candidates[newAgentVotes[agent]] += 1
        noVotesChanged = False

    self.ConvergenceReached = noVotesChanged

    # assign new votes to agents
    for agent in newAgentVotes:
      self.Agents[agent].currentVote = newAgentVotes[agent]

    # check for a consensus
    for candidate in self.Candidates:
      if self.Candidates[candidate] == len(self.Agents):
        self.WinningCandidate = candidate
        self.ConsensusReached = True
        self.ConvergenceReached = True
        break

    if self.ConvergenceReached and not self.ConsensusReached:
      # get the max votes of any candidate
      mostVotes = max(self.Candidates.values())
      # get all candidates with the max votes, this will catch ties
      possibleWinners = [key for key, val in self.Candidates.items() if val == mostVotes]
      # if a tie exists, randomly select a winner
      if len(possibleWinners) > 1:
        random.shuffle(possibleWinners)

      self.WinningCandidate = possibleWinners[0]

    if not self.ConvergenceReached and not self.ConsensusReached:
      if self.WeightDelta != 0:
        self.ChangeConnWeights()

  def ChangeConnWeights(self):
    logging.debug('Changing the weights...')
    for agent in self.Agents:
      for conn in self.Agents[agent].connections:
        temp = self.Agents[agent].connections[conn] + random.uniform(-self.WeightDelta, self.WeightDelta)
        self.Agents[agent].connections[conn] = temp
