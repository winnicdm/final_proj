# Consensus from Dynamic, Sparsely Connected, Distributed Iterative Voting
## Alex Kinnear, Ben Winslett, Ian McLachlan
## CS 5110

To use this script, simply run it with one of the available optional arguments

`python main.py <arg?>`

### Optional arguments:
`-h  --help`     Displays this usage message

`-d  --default`  Uses all default values for the script and skips setup

The designated file must be a csv file and must contain any number of rows with the following values, deliminated by a comma:

`agent_count              int`

`candidate_count          int`

`system_trust             float`

`connectivity_percentage  float`

`weight_change_severity   float`

`max_iteration            int`

### Dependencies:
`Python 3.X.X` The script could work with a lower version, but it was developed using 3

`tabulate`      Used to generate the dynamic tables seen throughout
```pip install tabulate```